using System;
using System.Collections.Generic;
using System.Linq;
using Core.Systems.UI.PopupSystem;
using Game.Inventory;
using Game.Towers;
using Game.UI.Popups.TowerLibraryPopup;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.UI.Popups
{
    public class UITowerLibraryPopup : BasePopup
    {
        [SerializeField] private TowerInventoryItemDataContainer _towerInventoryItemDataContainer;
        [SerializeField] private Transform _scrollContent;
        [SerializeField] private TowerLibraryScrollItemView _scrollItemViewPrefab;
        [SerializeField] private TowerLibraryInfoPanel _infoPanel;

        private readonly List<TowerLibraryScrollItemView> _itemViews = new();


        private void Awake()
        {
            var towerInventoryItems = _towerInventoryItemDataContainer.Data.ToList();
            var groupedItems = towerInventoryItems
                .GroupBy(item => item.TowerData).ToList();

            groupedItems.Sort(Comparison);

            foreach (var groupedItem in groupedItems)
            {
                var itemView = Instantiate(_scrollItemViewPrefab, _scrollContent);
                itemView.SetTowerData(groupedItem);
                _itemViews.Add(itemView);
            }

            TowerLibraryScrollItemView.Selected += TowerLibraryScrollItemViewOnSelected;

            if (_itemViews.Count > 0)
                _itemViews[0].Select();
        }

        private static int Comparison(IGrouping<TowerData, TowerInventoryItem> x,
            IGrouping<TowerData, TowerInventoryItem> y)
        {
            var xSum = Mathf.Clamp(x.Sum(i => i.Count), 0, 1);
            var ySum = Mathf.Clamp(y.Sum(i => i.Count), 0, 1);
            if (xSum != ySum)
                return ySum - xSum;

            if (xSum < 1 || ySum < 1)
                return string.Compare(x.Key.Name, y.Key.Name, StringComparison.Ordinal);

            var xMaxQualityLevel = x.Where(i => i.Count > 0).Max(i => i.QualityType.Level);
            var yMaxQualityLevel = y.Where(i => i.Count > 0).Max(i => i.QualityType.Level);

            if (xMaxQualityLevel != yMaxQualityLevel)
                return  yMaxQualityLevel - xMaxQualityLevel;
            
            return string.Compare(x.Key.Name, y.Key.Name, StringComparison.Ordinal);
        }

        private void TowerLibraryScrollItemViewOnSelected(IGrouping<TowerData, TowerInventoryItem> towerInventoryItems)
        {
            _infoPanel.SetInfo(towerInventoryItems);
        }
    }
}