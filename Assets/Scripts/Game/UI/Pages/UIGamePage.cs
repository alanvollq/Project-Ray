using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Enemies;
using Game.Systems.BuildSystem;
using Game.UI.Popups;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public sealed class UIGamePage : BasePage
    {
        [SerializeField] private Button _menuButton;
        [SerializeField] private Button _buildButton;
        [SerializeField] private EnemySpawner _enemySpawner; //TODO: remove after tests.
       

        private IPopupSystem _popupSystem;
        private IBuildSystem _buildSystem;


        protected override void OnInitialization()
        {
            _popupSystem = GameContext.GetSystem<IPopupSystem>();
            _buildSystem = GameContext.GetSystem<IBuildSystem>();
            
            _menuButton.onClick.AddListener(OnMenuButtonClick);
            _buildButton.onClick.AddListener(OnBuildButtonClick);
        }

        private void OnMenuButtonClick()
        {
            _popupSystem.Open<UIGameMenuPopup>();
        }

        private void OnBuildButtonClick()
        {
            _buildButton.gameObject.SetActive(false);
            _popupSystem.Open<UIBuildPopup>();
            _popupSystem.PopupClosed += PopupSystemOnPopupClosed;
        }

        private void PopupSystemOnPopupClosed(BasePopup popup)
        {
          if(popup.GetType() != typeof(UIBuildPopup))
              return;
          
          _buildButton.gameObject.SetActive(true);
        }

        protected override void OnBeforeClose()
        {
            _enemySpawner.DestroyAllEnemies();
            _buildSystem.DestroyAllBuilds();
        }
    }
}