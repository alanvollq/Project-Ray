using UnityEngine;

namespace Core.Extensions
{
    public static class MonoBehaviourExtension
    {
        public static void DestroyGO(this GameObject gameObject)
        {
            Object.Destroy(gameObject);
        }

        public static void DestroyGO(this Component component)
        {
            Object.Destroy(component.gameObject);
        }
    }
}