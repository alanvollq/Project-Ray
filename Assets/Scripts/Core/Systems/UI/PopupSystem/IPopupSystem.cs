using System;

namespace Core.Systems.UI.PopupSystem
{
    public interface IPopupSystem : ISystem
    {
        public event Action<BasePopup> PopupOpened;
        public event Action<BasePopup> PopupClosed;
        
        public void Open<TPopup>(IUIElementOpenParam openParam = null) where TPopup : BasePopup;
        public void CloseLast();
        public void CloseAll();
    }
}