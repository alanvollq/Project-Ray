using Game.QualityTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.TowerLibraryPopup
{
  
    public class QualityCountField : MonoBehaviour
    {
        [SerializeField] private TMP_Text _countText;
        [SerializeField] private Image _qualityColorPlane;
        [SerializeField] private QualityType _qualityType;


        public QualityType QualityType => _qualityType;


        public void SetCount(int count)
        {
            gameObject.SetActive(count > 0);
            _countText.SetText($"{count}");
        }

        [ContextMenu("SetColorByQuality")]
        public void SetColorByQuality()
        {
            _countText.color = _qualityType.Color;
            _qualityColorPlane.color = _qualityType.Color;
        }
    }
}