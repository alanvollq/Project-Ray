using Core.GameContext;
using UnityEngine;

namespace Core.Systems.UI.PageSystem
{
    public abstract class BasePage : MonoBehaviour
    {
        protected IPageSystem PageSystem;
        protected IGameContext GameContext;

        public RectTransform RectTransform { get; private set; }


        public BasePage Initialization(IPageSystem pageSystem, IGameContext gameContext)
        {
            PageSystem = pageSystem;
            GameContext = gameContext;

            RectTransform = GetComponent<RectTransform>();

            OnInitialization();

            gameObject.SetActive(false);

            return this;
        }

        protected virtual void OnInitialization()
        {
        }

        public void Open()
        {
            OnBeforeOpen();
            gameObject.SetActive(true);
            OnAfterOpen();
        }

        public void Close()
        {
            OnBeforeClose();
            gameObject.SetActive(false);
            OnAfterClose();
        }

        protected virtual void OnBeforeOpen()
        {
        }

        protected virtual void OnAfterOpen()
        {
        }

        protected virtual void OnBeforeClose()
        {
        }

        protected virtual void OnAfterClose()
        {
        }
    }
}