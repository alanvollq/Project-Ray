using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Levels;
using Game.UI.Popups;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Pages
{
    public sealed class UIMainPage : BasePage
    {
        [SerializeField] private Button _newGameButton;
        [SerializeField] private Button _settingsMenuButton;
        [SerializeField] private Button _statisticsMenuButton;
        [SerializeField] private Button _enemiesLibraryButton;
        [SerializeField] private Button _towersLibraryButton;
        [SerializeField] private LevelData _levelData;

        private IPopupSystem _popupSystem;

        
        protected override void OnInitialization()
        {
            _popupSystem = GameContext.GetSystem<IPopupSystem>();
            
            _newGameButton.onClick.AddListener(OnNewGameButton);
            _towersLibraryButton.onClick.AddListener(OnTowerLibraryButton);
            _enemiesLibraryButton.onClick.AddListener(OnEnemiesLibraryButton);
        }

        private void OnNewGameButton()
        {
            var openParam = new UILaunchLevelPopupOpenParam(_levelData);
            _popupSystem.Open<UILaunchLevelPopup>(openParam);
        }
        private void OnTowerLibraryButton()
        {
            _popupSystem.Open<UITowerLibraryPopup>();
        }

        private void OnEnemiesLibraryButton()
        {
            _popupSystem.Open<UIEnemiesLibraryPopup>();
        }
    }
}