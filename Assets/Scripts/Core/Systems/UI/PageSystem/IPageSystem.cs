using System;

namespace Core.Systems.UI.PageSystem
{
    public interface IPageSystem : ISystem
    {
        public event Action PageOpened;
        public event Action PageClosed;
        
        
        public void Open<TPage>() where TPage : BasePage;
    }
}