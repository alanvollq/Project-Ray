using Game.CreatureTypes;
using UnityEngine;

namespace Game.Abilities
{
    public interface IAbility
    {
        public CreatureType Type { get; }
        public string Name { get; }
        public Sprite Icon { get; }
    }
}