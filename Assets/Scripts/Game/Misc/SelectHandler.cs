using System;
using UnityEngine;

namespace Game.Misc
{
    public class SelectHandler : MonoBehaviour
    {
        public event Action Selected;
        public event Action Deselected;

        private static SelectHandler _currentSelected;

        public void Select()
        {
            if (_currentSelected == this)
                return;

            if (_currentSelected != null)
                _currentSelected.Deselect();

            Selected?.Invoke();
            _currentSelected = this;
        }

        public void Deselect()
        {
            Deselected?.Invoke();
        }
    }
}