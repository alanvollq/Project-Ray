using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Levels
{
    [Serializable]
    public class WaveData
    {
        [SerializeField] private List<WaveSpawnerData> _waveSpawnersData;


        public IEnumerable<WaveSpawnerData> WaveSpawnersData => _waveSpawnersData;
    }
}