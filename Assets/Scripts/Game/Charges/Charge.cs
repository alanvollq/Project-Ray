using Core.Loggers;
using DG.Tweening;
using Game.Enemies;
using Game.Systems.DamageSystem;
using UnityEngine;
using static Game.Data.Loggers.GameLogData;

namespace Game.Charges
{
    public class Charge : MonoBehaviour
    {
        [SerializeField] private float _speed;

        private Transform _transform;
        private Transform _target;
        private bool _isFollow;
        private Damage _damage;


        private void Awake()
        {
            _transform = transform;
        }

        private void OnDestroy()
        {
            _transform.DOKill();
        }

        private void Update()
        {
            if (_isFollow == false)
                return;

            if (_target == null)
            {
                Destroy(gameObject);
                return;
            }

            var position = _target.position;
            MoveToPosition(position);
        }

        public void SetDamage(Damage damage)
        {
            _damage = damage;
        }

        private void MoveToPosition(Vector3 targetPosition)
        {
            if (_speed == 0)
                return;


            var distance = Vector3.Distance(_transform.position, targetPosition);
            var duration = distance / _speed;

            _transform.DOKill();
            _transform.DOMove(targetPosition, duration)
                .SetEase(Ease.Linear)
                .OnComplete(() => Destroy(gameObject));
        }

        public void MoveToTarget(Transform target)
        {
            _target = target;
            _isFollow = true;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var enemy = other.GetComponent<Enemy>();
            if (enemy == null)
                return;

            ChargeLogData.Log($"{name} - On trigger enter: {other.name}");
            enemy.TakeDamage(_damage);
        }
    }
}