using System;
using Core.Data;
using UnityEngine;

namespace Game.Data.Loggers
{
    [CreateAssetMenu(fileName = "Game Log Data", menuName = "Data/Loggers/Game Log Data")]
    public class GameLogData : ScriptableObject
    {
        [SerializeField] private SOLogData _defaultLogData;
        [SerializeField] private UILogData _uiLogData;
        [SerializeField] private SOLogData _clickHandlerLogData;
        [SerializeField] private SOLogData _towerLogData;
        [SerializeField] private SOLogData _chargeLogData;
        [SerializeField] private SOLogData _enemyLogData;
        [SerializeField] private SOLogData _buildLogData;
        [SerializeField] private SOLogData _targetHandlerLogData;

        private static GameLogData _instance;


        public static SOLogData DefaultLogData => _instance._defaultLogData;
        public static UILogData UILogData => _instance._uiLogData;
        public static SOLogData ClickHandlerLogData => _instance._clickHandlerLogData;
        public static SOLogData TowerLogData => _instance._towerLogData;
        public static SOLogData ChargeLogData => _instance._chargeLogData;
        public static SOLogData EnemyLogData => _instance._enemyLogData;
        public static SOLogData BuildLogData => _instance._buildLogData;
        public static SOLogData TargetHandlerLogData => _instance._targetHandlerLogData;


        public void Init()
        {
            _instance = this;
        }
    }

    [Serializable]
    public class UILogData
    {
        [SerializeField] private SOLogData _logData;
        [SerializeField] private SOLogData _pageLogData;
        [SerializeField] private SOLogData _popupLogData;
        [SerializeField] private SOLogData _messageboxLogData;


        public SOLogData Default => _logData;
        public SOLogData Page => _pageLogData;
        public SOLogData Popup => _popupLogData;
        public SOLogData Messagebox => _messageboxLogData;
    }
}