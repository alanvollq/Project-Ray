using System;
using Core.Extensions;
using Core.Loggers;
using DG.Tweening;
using Game.Data.Loggers;
using Game.Systems.DamageSystem;
using UnityEngine;

namespace Game.Enemies
{
    public class Enemy : MonoBehaviour, IDamageable
    {
        public event Action<Enemy> Released;

        [SerializeField] private EnemyView _view;

        private EnemyStats _stats;
        private Transform _transform;


        public EnemyStats Stats => _stats;


        private void Awake()
        {
            _transform = transform;
        }

        public void Init(Vector3 startPoint, Vector3 targetPoint, EnemyStats stats)
        {
            _stats = new EnemyStats(stats);
            _transform.position = startPoint;
            _view.Init(_stats);
            Move(targetPoint);
        }

        private void Move(Vector3 position)
        {
            var distance = Vector3.Distance(_transform.position, position);
            var duration = distance / _stats.Speed;
            _transform.DOMove(position, duration)
                .SetEase(Ease.Linear)
                .OnComplete(() => Destroy(gameObject))
                ;
        }

        public DamageResult TakeDamage(Damage damage)
        {
            var prevHealth = _stats.CurrentHealth;
            _stats.SetValue(_stats.CurrentHealth - damage.Value);
            var damageResult = new DamageResult
            {
                Damage = prevHealth - _stats.CurrentHealth
            };
            GameLogData.EnemyLogData.Log($"{name} take " + $"{damageResult.Damage}".Bold().Color(Color.red) + " damage" +
                                         $"|| Health: {_stats.CurrentHealth}/{_stats.MaxHealth}");

            if (_stats.CurrentHealth <= 0)
                Destroy(gameObject);

            damage.Owner.AddDamageResult(damageResult);
            return damageResult;
        }

        private void OnDestroy()
        {
            Released?.Invoke(this);
        }
    }
}