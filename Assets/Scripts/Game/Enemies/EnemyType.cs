using Game.CreatureTypes;
using UnityEngine;

namespace Game.Enemies
{
    [CreateAssetMenu(fileName = "Enemy Data", menuName = "Data/Game/Enemies/Enemy Type")]
    public class EnemyType : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private string _description;
        [SerializeField] private CreatureType _creatureType;


        public string Name => _name;
        public string Description => _description;
        public CreatureType CreatureType => _creatureType;
    }
}