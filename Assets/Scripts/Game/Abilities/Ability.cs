using Game.CreatureTypes;
using UnityEngine;

namespace Game.Abilities
{
    public abstract class Ability : ScriptableObject, IAbility
    {
        [SerializeField] private CreatureType _type;
        [SerializeField] private string _name;
        [SerializeField] private Sprite _icon;


        public CreatureType Type => _type;
        public string Name => _name;
        public Sprite Icon => _icon;
    }
}