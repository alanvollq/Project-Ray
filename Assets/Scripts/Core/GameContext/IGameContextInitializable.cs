namespace Core.GameContext
{
    public interface IGameContextInitializable
    {
        public void Initialization(IGameContext gameContext);
    }
}