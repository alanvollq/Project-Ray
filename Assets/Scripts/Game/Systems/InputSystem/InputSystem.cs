﻿using System;
using Core.GameContext;
using Game.Systems.CameraSystem;
using UnityEngine;

namespace Game.Systems.InputSystem
{
    public class InputSystem : IInputSystem, IGameContextInitializable
    {
        public event Action<float> OnZoomStarted;

        private readonly PlayerInputActions _playerInputActions;

        private Vector2 _deltaMove;
        private Vector2 _worldPosition;
        private Vector2 _screenPosition;
        private Vector2 _worldTouchPosition;
        private Vector2 _primaryTouchPosition;
        private float _lastTouchDistance;
        private int _touchZoom;

        private ICameraSystem _cameraSystem;


        public Vector2 DeltaMove => _deltaMove;

        public Vector2 WorldPosition => _worldPosition;

        public Vector2 ScreenPosition => _screenPosition;

        public Vector2 WorldTouchPosition => _worldTouchPosition;

        public InputSystem()
        {
            _playerInputActions = new PlayerInputActions();
        }

        public void Initialization(IGameContext gameContext)
        {
            _cameraSystem = gameContext.GetSystem<ICameraSystem>();
#if UNITY_EDITOR
            InitOnEditor();
#endif

#if UNITY_ANDROID
            InitOnAndroid();
#endif

            Enable();
        }

        public void Enable()
        {
            _playerInputActions.Enable();
        }

        public void Disable()
        {
            _playerInputActions.Disable();
        }

        private void TouchZoomUpdate(Vector2 secondaryTouchPosition)
        {
            var touchDistance = Vector2.Distance(_primaryTouchPosition, secondaryTouchPosition);

            if (touchDistance < _lastTouchDistance)
                _touchZoom = 1;
            else if (touchDistance > _lastTouchDistance)
                _touchZoom = -1;
            else
                _touchZoom = 0;

            _lastTouchDistance = touchDistance;
            OnZoomStarted?.Invoke(_touchZoom);
        }

        private void InitOnAndroid()
        {
            _playerInputActions.Touch.Movement.performed += i => { _deltaMove = i.ReadValue<Vector2>(); };
            _playerInputActions.Touch.PrimaryPosition.performed +=
                i =>
                {
                    var position = i.ReadValue<Vector2>();
                    _screenPosition = position;
                    _worldPosition = _cameraSystem.MainCamera.ScreenToWorldPoint(position);
                };

            _playerInputActions.Touch.SecondaryPosition.started += i =>
                _lastTouchDistance = Vector2.Distance(_primaryTouchPosition, i.ReadValue<Vector2>());
            _playerInputActions.Touch.SecondaryPosition.performed += i =>
                TouchZoomUpdate(i.ReadValue<Vector2>());
        }

        private void InitOnEditor()
        {
            _playerInputActions.Mouse.Zoom.started += i => OnZoomStarted?.Invoke(-i.ReadValue<float>());
            _playerInputActions.Mouse.Movement.performed += i => _deltaMove = i.ReadValue<Vector2>();
            _playerInputActions.Mouse.Position.performed +=
                i =>
                {
                    var position = i.ReadValue<Vector2>();
                    _screenPosition = position;
                    _worldPosition = _cameraSystem.MainCamera.ScreenToWorldPoint(position);
                };
        }
    }
}