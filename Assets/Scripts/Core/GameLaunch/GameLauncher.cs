using System.Collections.Generic;
using Core.Data;
using Core.Loggers;
using Core.Root;
using Game.Misc;
using Game.Systems.CameraSystem;
using Game.Systems.InputSystem;
using UnityEngine;

namespace Core.GameLaunch
{
    public class GameLauncher : MonoBehaviour
    {
        [SerializeField] private SOLogData _launchLogData;
        [SerializeField] private RootOwner _root;
        [SerializeField] private List<LaunchTask> _launchTasks;

        [SerializeField] private CameraController _cameraController;

        private void Awake()
        {
            var gameContext = new GameContext.GameContext(_root);

            foreach (var task in _launchTasks)
            {
                _launchLogData.Log($"Started - {task.name}");
                task.Execute(gameContext);
                _launchLogData.Log($"Completed - {task.name}");
            }

            _cameraController.Init(gameContext.GetSystem<IInputSystem>(), gameContext.GetSystem<ICameraSystem>());
            _cameraController.OnLevelLoaded(new Vector2(-20, -20), new Vector2(20, 20));
            Destroy(gameObject);
        }
    }
}