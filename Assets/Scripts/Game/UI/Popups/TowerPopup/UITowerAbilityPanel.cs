using Core.UIElements;
using Game.Abilities;
using Game.Towers;
using UnityEngine;

namespace Game.UI.Popups.TowerPopup
{
    public class UITowerAbilityPanel : UIPanel
    {
        [SerializeField] private AbilitySlotView _weaponAbilitySlotView;
        [SerializeField] private AbilitySlotView _passiveAbilitySlotView;
        [SerializeField] private AbilitySlotView _activeAbilitySlotView;


        public void SetTower(TowerController tower)
        {
            _weaponAbilitySlotView.SetInfo(tower.Abilities.WeaponAbilitySlot.ActiveAbility);
            _passiveAbilitySlotView.SetInfo(tower.Abilities.PassiveAbilitySlot.ActiveAbility);
            _activeAbilitySlotView.SetInfo(tower.Abilities.ActiveAbilitySlot.ActiveAbility);
        }
    }
}