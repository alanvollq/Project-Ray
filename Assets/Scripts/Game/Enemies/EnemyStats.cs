using System;
using UnityEngine;

namespace Game.Enemies
{
    [Serializable]
    public class EnemyStats
    {
        public event Action HealthChanged;

        [SerializeField] private float _maxHealth;
        [SerializeField] private float _speed;

        private float _currentHealth;

        public EnemyStats(EnemyStats stats)
        {
            _speed = stats.Speed;
            _maxHealth = stats.MaxHealth;
            
            SetValue(_maxHealth);
        }


        public float MaxHealth => _maxHealth;
        public float CurrentHealth => _currentHealth;
        public float Speed => _speed;
        public float PercentCurrentValue => Math.Clamp((_currentHealth / _maxHealth), 0f, 1);


        public void SetValue(float value)
        {
            _currentHealth = value;
            _currentHealth = Math.Clamp(_currentHealth, 0f, _maxHealth);
            
            HealthChanged?.Invoke();
        }
    }
}