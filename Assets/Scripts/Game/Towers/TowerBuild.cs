using Game.Systems.BuildSystem;
using UnityEngine;

namespace Game.Towers
{
    public class TowerBuild : Build
    {
        [SerializeField] private TowerController _towerController;


        protected override void OnInit()
        {
            _towerController.SetTowerData(null);
            _towerController.Init(GameContext);
        }
    }
}