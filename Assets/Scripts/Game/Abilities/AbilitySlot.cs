using System;
using UnityEngine;

namespace Game.Abilities
{
    [Serializable]
    public class AbilitySlot<T> where T : Ability
    {
        [SerializeField] private T _firstAbility;
        [SerializeField] private T _secondAbility;

        private T _activeAbility;


        public T FirstAbility => _firstAbility;
        public T SecondAbility => _secondAbility;
        public T ActiveAbility => _activeAbility == null? _firstAbility: _activeAbility;


        public void SwitchAbility()
        {
            if (_activeAbility == null)
                _activeAbility = _firstAbility;
            if (_activeAbility == _firstAbility)
                _activeAbility = _secondAbility;
            else
                _secondAbility = _activeAbility;
        }
    }
}