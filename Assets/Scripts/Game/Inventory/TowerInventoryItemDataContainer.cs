using Core.Data;
using UnityEngine;

namespace Game.Inventory
{
    [CreateAssetMenu(fileName = "Tower Inventory Item Data Container", menuName = "Data/Game/Inventory/Tower Inventory Item Data Container", order = 0)]
    public class TowerInventoryItemDataContainer : DataContainer<TowerInventoryItem>
    {
        
    }
}