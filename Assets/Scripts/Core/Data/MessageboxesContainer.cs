using Core.Systems.UI.MessageboxSystem;
using UnityEngine;

namespace Core.Data
{
    [CreateAssetMenu(fileName = "Messageboxes Container", menuName = "Data/Core/Managers/UI/Messageboxes Container")]
    public class MessageboxesContainer : DataContainer<BaseMessagebox> { }
}