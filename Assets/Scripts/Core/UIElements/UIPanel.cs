using UnityEngine;

namespace Core.UIElements
{
    public class UIPanel : MonoBehaviour
    {
        public void SetEnable(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}