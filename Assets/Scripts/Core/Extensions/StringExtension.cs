using UnityEngine;

namespace Core.Extensions
{
    public static class StringExtension
    {
        public static string Bold(this string value)
        {
            return string.IsNullOrEmpty(value) 
                ? string.Empty 
                : $"<b>{value}</b> ";
        }

        public static string Color(this string value, Color color)
        {
            return string.IsNullOrEmpty(value)
                ? string.Empty
                : $"<color=#{ColorUtility.ToHtmlStringRGB(color)}>{value}</color>";
        }
    }
}