using Core.UIElements;
using Game.Systems.DamageSystem;
using Game.Towers;
using TMPro;
using UnityEngine;

namespace Game.UI.Popups.TowerPopup
{
    public class UITowerStatisticPanel : UIPanel
    {
        [SerializeField] private TMP_Text _allDamage;
        private TowerController _tower;
        

        public void SetTower(TowerController tower)
        {
            _tower = tower;
        }

        private void OnEnable()
        {
            _tower.OnDealDamage += TowerOnOnDealDamage;
            SetDamage();
        }

        private void OnDisable()
        {
            _tower.OnDealDamage -= TowerOnOnDealDamage;
        }

        private void TowerOnOnDealDamage(DamageResult damageResult)
        {
            SetDamage();
        }

        private void SetDamage()
        {
            _allDamage.text = _tower.AllDamage.ToString("F");
        }
    }
}