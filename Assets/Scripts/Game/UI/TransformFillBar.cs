using System;
using UnityEngine;

namespace Game.UI
{
    public class TransformFillBar : MonoBehaviour
    {
        [SerializeField] private Transform _foreground;
        [SerializeField] private Direction _direction;


        public void SetValue(float value)
        {
            value = Mathf.Clamp(value, 0, 1);
            
            var newScale = _foreground.localScale;
            switch (_direction)
            {
                case Direction.X:
                    newScale.x = value;
                    break;
                case Direction.Y:
                    newScale.y = value;
                    break;
                case Direction.Z:
                    newScale.z = value;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _foreground.localScale = newScale;
        }


        [Serializable] private enum Direction { X, Y, Z }
    }
}