using NaughtyAttributes;
using System.Collections.Generic;
using Core.Extensions;
using UnityEngine;

namespace Game.Enemies
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private EnemyData _enemyData;
        [SerializeField] private float _spawnInterval;
        [SerializeField] private bool _canSpawn = true;

        private float _lastTimeToSpawn;
        private Transform _enemyParent;
        private readonly List<Enemy> _enemies = new();


        private void Awake()
        {
            _lastTimeToSpawn = 0;
        }

        private void Update()
        {
            _lastTimeToSpawn -= Time.deltaTime;
            if (_lastTimeToSpawn > 0)
                return;

            if (_canSpawn == false)
                return;

            SpawnEnemy();
        }

        [Button]
        private void SpawnEnemy()
        {
            _lastTimeToSpawn = _spawnInterval;
            var enemy = Instantiate(_enemyData.Prefab, _enemyParent, true);
            enemy.Released += OnEnemyReleased;
            enemy.Init(new Vector3(-6, 0), new Vector3(6, 0), _enemyData.Stats);
            _enemies.Add(enemy);
        }

        private void OnEnemyReleased(Enemy enemy)
        {
            _enemies.Remove(enemy);
        }

        public void DestroyAllEnemies()
        {
            _enemies.ForEach(enemy => enemy.DestroyGO());
            _enemies.Clear();
        }
    }
}