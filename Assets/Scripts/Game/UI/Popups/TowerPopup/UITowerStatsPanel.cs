using Core.UIElements;
using Game.Towers;
using TMPro;
using UnityEngine;

namespace Game.UI.Popups.TowerPopup
{
    public class UITowerStatsPanel : UIPanel
    {
        [SerializeField] private TMP_Text _rangeValue;
        [SerializeField] private TMP_Text _damageValue;
        [SerializeField] private TMP_Text _attackSpeedValue;
        [SerializeField] private GameObject _additionalStatPanel;


        public void SetInfo(WeaponStats weaponStats)
        {
            _additionalStatPanel.SetActive(false);
            _rangeValue.text = weaponStats.Range.ToString("F");
            _damageValue.text = weaponStats.Damage.ToString("F");
            _attackSpeedValue.text = weaponStats.AttackSpeed.ToString("F") + " /s.";
        }
    }
}