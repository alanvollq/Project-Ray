using UnityEngine;

namespace Game.QualityTypes
{
    [CreateAssetMenu(fileName = "Types Of Quality", menuName = "Data/Game/Types Of Quality")]
    public class QualityType : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private Color _color;
        [SerializeField] private int _level;


        public string Name => _name;
        public Color Color => _color;
        public int Level => _level;
    }
}