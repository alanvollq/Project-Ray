using Core.GameContext;
using Game.Misc;
using UnityEngine;

namespace Game.Towers
{
    public class TowerView : MonoBehaviour
    {
        [SerializeField] private Transform _radiusView;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private TowerController _towerController;
        [SerializeField] private ClickHandler _clickHandler;

        private ITowerManager _towerManager;
        private Color _defaultColor;


        private void Awake()
        {
            _clickHandler.Down += OnDown;
            
            _defaultColor = _spriteRenderer.color;
            _spriteRenderer.color = Color.clear;
        }

        public void Init(TowerController towerController, IGameContext gameContext)
        {
            _towerController = towerController;
            
            _towerManager = gameContext.GetManager<ITowerManager>();
        }

        private void OnDown()
        {
            _towerManager.Select(_towerController);
        }

        public void SetRadiusEnable(bool value)
        {
            _spriteRenderer.color = value ? _defaultColor : Color.clear;
            _spriteRenderer.enabled = value;
        }

        public void SetRadiusScale(float value)
        {
            value = value * 2 + 1;
            _radiusView.localScale = new Vector3(value, value, 1);
        }
    }
}