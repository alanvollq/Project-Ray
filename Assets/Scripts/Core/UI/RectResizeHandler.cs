using TMPro;
using UnityEngine;

namespace Core.UI
{
    public class RectResizeHandler : MonoBehaviour
    {
        [SerializeField] private TMP_Text _tmpText;
        [SerializeField] private RectTransform _transform;
        [SerializeField] private Vector2 _padding;
        [SerializeField] private bool _updateSize;
        [SerializeField] private bool _updatePosition;

        private void Awake()
        {
            if (_transform == null)
                _transform = GetComponent<RectTransform>();
        }

        private void Update()
        {
            if (_updateSize)
                SetPreferredSize();

            if (_updatePosition)
                SetPositionFromText();
        }

        [ContextMenu("SetPreferredSize")]
        public void SetPreferredSize()
        {
            var preferredWidth = _tmpText.preferredWidth;
            var preferredHeight = _tmpText.preferredHeight;

            var size = _transform.sizeDelta;
            size.x = preferredWidth + _padding.x * 2;
            size.y = preferredHeight + _padding.y * 2;

            _transform.sizeDelta = size;
        }

        [ContextMenu("SetPositionFromText")]
        public void SetPositionFromText()
        {
            _transform.position = _tmpText.transform.position;
        }
    }
}