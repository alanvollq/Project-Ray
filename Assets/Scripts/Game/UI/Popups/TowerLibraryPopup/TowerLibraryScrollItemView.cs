using System;
using System.Collections.Generic;
using System.Linq;
using Game.Inventory;
using Game.Towers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.TowerLibraryPopup
{
    public class TowerLibraryScrollItemView : MonoBehaviour
    {
        public static event Action<IGrouping<TowerData, TowerInventoryItem>> Selected;

        [SerializeField] private Button _button;
        [SerializeField] private GameObject _lockObject;
        [SerializeField] private GameObject _countField;

        [SerializeField] private TMP_Text _towerName;
        [SerializeField] private Image _towerIcon;
        [SerializeField] private Image _typeIcon;
        [SerializeField] private List<QualityCountField> _qualityCountFields;
        [SerializeField] private Image _border;


        private IGrouping<TowerData, TowerInventoryItem> _towerInventoryItem;


        private void Awake()
        {
            _button.onClick.AddListener(Select);
            _qualityCountFields.ForEach(item => item.SetColorByQuality());
        }

        public void Select()
        {
            Selected?.Invoke(_towerInventoryItem);
        }

        public void SetTowerData(IGrouping<TowerData, TowerInventoryItem> groupingItem)
        {
            _towerInventoryItem = groupingItem;

            var towerData = groupingItem.Key;

            _towerName.text = towerData.Name;
            _towerIcon.sprite = towerData.Icon;
            _typeIcon.sprite = towerData.CreatureType.Icon;

            var allCount = groupingItem.Sum(item => item.Count);
            _lockObject.SetActive(allCount == 0);
            _countField.SetActive(allCount > 0);
            if (allCount < 1)
                return;

            foreach (var towerInventoryItem in groupingItem)
            {
                var qualityCountField =
                    _qualityCountFields.First(item => item.QualityType == towerInventoryItem.QualityType);
                qualityCountField.SetCount(towerInventoryItem.Count);
            }

            _border.color = _qualityCountFields
                .OrderBy(item => item.QualityType.Level)
                .Last(item => item.gameObject.activeSelf)
                .QualityType.Color;
        }
    }
}