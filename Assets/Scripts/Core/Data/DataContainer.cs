using System.Collections.Generic;
using UnityEngine;

namespace Core.Data
{
    public abstract class DataContainer<TData> : ScriptableObject
    {
        [SerializeField] private List<TData> _data;

        
        public IEnumerable<TData> Data => _data;
    }
}