using UnityEngine;

namespace Core.Root
{
    public interface IRootOwner
    {
        public Transform ParentRoot { get; }
        public Transform CoreRoot { get; }
        public Transform GameRoot { get; }
        public RectTransform UIRoot { get; }
    }
}