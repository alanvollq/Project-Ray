using Core.GameContext;
using Core.Systems.UI.MessageboxSystem;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;

namespace Core.Systems.UI
{
    public class UISystemManager : IUISystemManager, IGameContextInitializable
    {
        private IPageSystem _pageSystem;
        private IPopupSystem _popupSystem;
        private IMessageboxSystem _messageboxSystem;
        
        
        public void Initialization(IGameContext gameContext)
        {
            _pageSystem = gameContext.GetSystem<IPageSystem>();
            _popupSystem = gameContext.GetSystem<IPopupSystem>();
            _messageboxSystem = gameContext.GetSystem<IMessageboxSystem>();
            
            
            _pageSystem.PageClosed += PageSystemOnPageClosed;
        }

        private void PageSystemOnPageClosed()
        {
            _popupSystem.CloseAll();
        }
    }
}