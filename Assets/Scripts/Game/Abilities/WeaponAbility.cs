using Game.Charges;
using Game.Towers;
using UnityEngine;

namespace Game.Abilities
{
    [CreateAssetMenu(fileName = "Weapon Ability", menuName = "Data/Game/Abilities/Weapon Ability")]
    public class WeaponAbility : Ability
    {
        [SerializeField] private WeaponStats _stats;
        [SerializeField] private Charge _chargePrefab;

        
        public WeaponStats Stats => _stats;
    }
}