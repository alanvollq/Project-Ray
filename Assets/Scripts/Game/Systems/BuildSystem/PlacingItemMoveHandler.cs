﻿using System;
using DG.Tweening;
using Game.Misc;
using Game.Systems.CameraSystem;
using Game.Systems.InputSystem;
using UnityEngine;

namespace Game.Systems.BuildSystem
{
    [Serializable]
    public sealed class PlacingItemMoveHandler
    {
        [SerializeField] private DragHandler _dragHandler;
        [SerializeField] private ClickHandler _clickHandler;
        [SerializeField, Min(0.01f)] private float _step = 1;
        [SerializeField] private Vector3 _offset;
        [SerializeField] private float _moveDuration = 0.1f;

        private Transform _transform;
        private IInputSystem _inputSystem;
        private int _currentRotateAngle;
        private bool _isRotating;
        private float _touchDistance;
        private Vector3 _touchOffset;


        public void Init(IInputSystem inputSystem, ICameraSystem cameraSystem, Transform transform)
        {
            _inputSystem = inputSystem;
            _transform = transform;

            _dragHandler.Dragged += OnDragged;
            _clickHandler.Down += OnDown;

            SetPosition(cameraSystem.CameraPosition, true);
        }

        private void OnDragged() => SetPosition(_inputSystem.WorldPosition);

        private void OnDown()
        {
           
            Vector3 touchPosition = _inputSystem.WorldPosition;
            _touchOffset = _transform.position - touchPosition + _offset;

            SetPosition(_inputSystem.WorldPosition);
        }

        private Vector3 CorrectPosition(Vector3 position)
        {
            position += new Vector3(_touchOffset.x, _touchOffset.y);
            return new Vector3
            {
                x = Mathf.Round(position.x / _step) * _step,
                y = Mathf.Round(position.y / _step) * _step,
                z = _transform.position.z
            };
        }

        private void SetPosition(Vector3 position, bool isForce = false)
        {
            position = CorrectPosition(position);

            if (_inputSystem.WorldPosition.x > position.x)
                position.x += _offset.x;
            else if (_inputSystem.WorldPosition.x < position.x)
                position.x -= _offset.x;

            if (_inputSystem.WorldPosition.y > position.y)
                position.y += _offset.y;
            else if (_inputSystem.WorldPosition.y < position.y)
                position.y -= _offset.y;

            if (isForce)
                _transform.position = position;
            else
            {
                _transform.DOKill();
                _transform.DOMove(position, _moveDuration).SetEase(Ease.Linear);
            }
        }

        public void Rotate()
        {
        }
    }
    
    public class Draggable : MonoBehaviour
    {
        private Vector3 offset;
        private float zCoord;
        private bool isDragging;
        public float step = 0.5f; // Задайте шаг перемещения здесь
        public Vector3 centerOffset = Vector3.zero; // Задайте смещение центра здесь

        void OnMouseDown()
        {
            isDragging = true;
            zCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
            offset = gameObject.transform.position - GetMouseWorldPos() + centerOffset;
        }

        void OnMouseUp()
        {
            isDragging = false;
        }

        void Update()
        {
            if (isDragging)
            {
                Vector3 newPosition = GetMouseWorldPos() + offset;
                // Округлите координаты до ближайшего шага
                float x = Mathf.Round(newPosition.x / step) * step;
                float y = Mathf.Round(newPosition.y / step) * step;
                float z = Mathf.Round(newPosition.z / step) * step;
                transform.position = new Vector3(x, y, z);
            }
        }

        private Vector3 GetMouseWorldPos()
        {
            Vector3 mousePoint = Input.mousePosition;
            mousePoint.z = zCoord;
            return Camera.main.ScreenToWorldPoint(mousePoint);
        }
    }
}