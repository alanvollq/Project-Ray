using Core.Data;
using UnityEngine;

namespace Game.Towers
{
    [CreateAssetMenu(fileName = "Towers Data Container", menuName = "Data/Game/Towers/Towers Data Container")]
    public class TowersDataContainer : DataContainer<TowerData>
    {
        
    }
}