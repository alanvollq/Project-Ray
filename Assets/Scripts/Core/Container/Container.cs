using System;
using System.Collections.Generic;

namespace Core.Container
{
    public class Container<TType> : IContainer<TType>
    {
        private static readonly Dictionary<Type, TType> _items = new();


        public void Add<TItem>(TItem item) where TItem : TType
        {
            var key = typeof(TItem);
            if (_items.ContainsKey(key))
                throw new Exception($"Items '{key}' is exist");

            _items.Add(key, item);
        }

        public TItem Get<TItem>() where TItem : TType
        {
            var key = typeof(TItem);
            if (!_items.TryGetValue(key, out var item))
                throw new Exception($"Item '{key}' is not exist");

            return (TItem)item;
        }
    }
}