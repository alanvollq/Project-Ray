using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Abilities
{
    public class AbilitySlotView : MonoBehaviour
    {
        [SerializeField] private Image _background;
        [SerializeField] private Image _icon;
        [SerializeField] private Image _advantageIcon;
        [SerializeField] private Image _disadvantageIcon;
        [SerializeField] private TMP_Text _label;


        public void SetInfo(Ability ability)
        {
            _label.text = ability.Name;
            _icon.sprite = ability.Icon;
            
            var type = ability.Type;
            _background.sprite = type.Icon;
            _advantageIcon.sprite = type.Advantage.Icon;
            _disadvantageIcon.sprite = type.Disadvantage.Icon;
        }
    }
}