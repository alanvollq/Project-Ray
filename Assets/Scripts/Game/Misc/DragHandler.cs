using System;
using Core.Loggers;
using Game.Misc.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Misc
{
    public sealed class DragHandler : MonoBehaviour, IDraggable, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public event Action DragStarted;
        public event Action Dragged;
        public event Action DragEnded;


        void IDragHandler.OnDrag(PointerEventData eventData) => Dragged?.Invoke();

        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
        {
            ConsoleLogger.Log("Begin Drag");
            DragStarted?.Invoke();
        }

        void IEndDragHandler.OnEndDrag(PointerEventData eventData) => DragEnded?.Invoke();
    }
}