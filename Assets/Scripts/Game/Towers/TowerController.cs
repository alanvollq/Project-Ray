using System;
using Core.GameContext;
using UnityEngine;
using Core.Loggers;
using Game.Abilities;
using Game.CreatureTypes;
using Game.Systems.DamageSystem;
using static Game.Data.Loggers.GameLogData;

namespace Game.Towers
{
    public class TowerController : MonoBehaviour, IDamageDealer
    {
        [SerializeField] CreatureType _type;
        [SerializeField] private TowerView _towerView;
        [SerializeField] private RadiusHandler _radiusHandler;
        [SerializeField] private AbilitySlots _abilities;
        [SerializeField] private Sprite _icon;
        
        private float _attackLeftTime;
        private float _allDamage;
        private Weapon _weapon;
        private TowerData _towerData;


        public WeaponStats WeaponStats => _abilities.WeaponAbilitySlot.ActiveAbility.Stats;
        public float AllDamage => _allDamage;
        public CreatureType Type => _type;

        public AbilitySlots Abilities => _abilities;

        public Sprite Icon
        {
            get => _icon;
            set => _icon = value;
        }


        public void Init(IGameContext gameContext)
        {
            _towerView.Init(this, gameContext);
            _abilities.Init();
            _weapon = new Weapon(_abilities.WeaponAbilitySlot.ActiveAbility, _radiusHandler, this, transform);
        }

        private void Update()
        {
            _weapon.Update();
        }

        public void Selected()
        {
            _towerView.SetRadiusEnable(true);
            TowerLogData.Log($"{name} - select");
        }

        public void Deselected()
        {
            _towerView.SetRadiusEnable(false);
            TowerLogData.Log($"{name} - deselect");
        }

        public void SetTowerData(TowerData towerData)
        {
            _towerData = towerData;
            _towerView.SetRadiusScale(WeaponStats.Range);
        }

        public event Action<DamageResult> OnDealDamage;

        public void AddDamageResult(DamageResult damageResult)
        {
            //TODO прокинуть урон через систему урона, а не через диллера.
            _allDamage += damageResult.Damage;
            OnDealDamage?.Invoke(damageResult);
        }
    }
}