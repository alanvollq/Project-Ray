using System;
using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.Levels;
using Game.UI.Pages;
using Game.UI.Popups.LaunchLevelPopup;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public class UILaunchLevelPopup : BasePopup
    {
        [SerializeField] private Button _launchLevelButton;
        [SerializeField] private UILaunchLevelPopupEnemiesPanel _launchLevelPopupEnemiesPanel;
        [SerializeField] private UILaunchLevelPopupTowersPanel _launchLevelPopupTowersPanel;
        [SerializeField] private UILaunchLevelPopupLevelInfoPanel _launchLevelPopupLevelInfoPanel;

        private IPageSystem _pageSystem;


        protected override void OnInitialization()
        {
            _pageSystem = GameContext.GetSystem<IPageSystem>();
            _launchLevelButton.onClick.AddListener(OnLaunchLevelButtonDown);
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            if (openParam is not UILaunchLevelPopupOpenParam launchLevelOpenParam)
                throw new Exception("open param is empty");

            var levelData = launchLevelOpenParam.LevelData;

            _launchLevelPopupEnemiesPanel.SetInfo(levelData.LevelWavesData.GetEnemies());
            _launchLevelPopupTowersPanel.SetInfo();
            _launchLevelPopupLevelInfoPanel.SetInfo(levelData);
        }

        private void OnLaunchLevelButtonDown()
        {
            _pageSystem.Open<UIGamePage>();
        }
    }

    public class UILaunchLevelPopupOpenParam : IUIElementOpenParam
    {
        public UILaunchLevelPopupOpenParam(LevelData levelData)
        {
            LevelData = levelData;
        }

        public LevelData LevelData { get; private set; }
    }
}