using Core.Systems.UI;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.UI.Pages;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public class UIResultLevelPopup : BasePopup
    {
        [SerializeField] private Button _exitToMenuButton;

        private IPageSystem _pageSystem;

        
        protected override void OnInitialization()
        {
            _pageSystem = GameContext.GetSystem<IPageSystem>();
            _exitToMenuButton.onClick.AddListener(OnExitToMenuButtonDown);
        }

        private void OnExitToMenuButtonDown()
        {
           _pageSystem.Open<UIMainPage>();
        }
    }
    
    public class UIResultLevelPopupOpenParam : IUIElementOpenParam
    {
        public UIResultLevelPopupOpenParam()
        { 
        }
    }
}