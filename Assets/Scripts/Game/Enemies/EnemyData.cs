using Game.QualityTypes;
using UnityEngine;

namespace Game.Enemies
{
    [CreateAssetMenu(fileName = "Enemy Data", menuName = "Data/Game/Enemies/Enemy Data")]
    public class EnemyData : ScriptableObject
    {
        [SerializeField] private EnemyType _enemyType;
        [SerializeField] private Sprite _icon;
        [SerializeField] private QualityType _qualityType;
        [SerializeField] private EnemyStats _stats;
        [SerializeField] private Enemy _prefab;


        public EnemyType EnemyType => _enemyType;
        public Sprite Icon => _icon;
        public QualityType QualityType => _qualityType;
        public EnemyStats Stats => _stats;
        public Enemy Prefab => _prefab;
    }
}