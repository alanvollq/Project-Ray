using Game.Systems.CameraSystem;
using Game.Systems.InputSystem;
using UnityEngine;

namespace Game.Misc
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private DragHandler _dragHandler;

        private IInputSystem _inputSystem;
        private ICameraSystem _cameraSystem;

        private Vector2 _previousInputScreenPosition;
        private Vector2 _previousInputWorldPosition;
        private Vector2 _startDragPos;
        private Vector3 _startCamPos;
        private Vector3 _clampDiff;
        private bool _canMove;

        private float _maxXBound;
        private float _maxYBound;
        private float _minXBound;
        private float _minYBound;
        private float _maxZoom = 10;
        private float _minZoom = 2;
        private float _maxZoomAvailable;
        private float _zoomSpeed = 1;

        
        public void Init(IInputSystem inputSystem, ICameraSystem cameraSystem)
        {
            _inputSystem = inputSystem;
            _cameraSystem = cameraSystem;

            _dragHandler.DragStarted += OnDragHandlerBeginDrag;
            _dragHandler.DragEnded += OnDragHandlerEndDrag;
            _dragHandler.Dragged += OnDragHandlerDrag;

            _inputSystem.OnZoomStarted += OnZoomStarted;
        }

        private void OnZoomStarted(float zoomValue)
        {
            if (zoomValue > 0)
            {
                if (_maxZoomAvailable < _maxZoom)
                    _maxZoom = _maxZoomAvailable;

                if (Mathf.Approximately(_cameraSystem.OrthographicSize, _maxZoom))
                    return;
            }

            var newZoom = _cameraSystem.OrthographicSize + zoomValue * Time.fixedDeltaTime * _zoomSpeed;
            _cameraSystem.SetOrthographicSize(Mathf.Clamp(newZoom, _minZoom, _maxZoom));

            var clampPosition = _cameraSystem.CameraPosition;
            clampPosition.x =
                Mathf.Clamp(clampPosition.x, _minXBound + _cameraSystem.Zoom * _cameraSystem.ScaleFactor,
                    _maxXBound - _cameraSystem.Zoom * _cameraSystem.ScaleFactor);
            clampPosition.y =
                Mathf.Clamp(clampPosition.y, _minYBound + _cameraSystem.Zoom, _maxYBound - _cameraSystem.Zoom);
            _cameraSystem.SetCameraPosition(clampPosition);
        }

        private void Update()
        {
            if (!_canMove)
                return;

            if (_previousInputWorldPosition == _inputSystem.WorldPosition)
                return;

            Vector3 direction = _inputSystem.WorldPosition - _startDragPos;
            var offsetCamera = _cameraSystem.CameraPosition - _startCamPos;
            direction -= offsetCamera;
            var newPosition = _startCamPos - direction;
            var clampPosition = newPosition;
            clampPosition.x =
                Mathf.Clamp(newPosition.x, _minXBound + _cameraSystem.Zoom * _cameraSystem.ScaleFactor,
                    _maxXBound - _cameraSystem.Zoom * _cameraSystem.ScaleFactor);
            clampPosition.y =
                Mathf.Clamp(newPosition.y, _minYBound + _cameraSystem.Zoom, _maxYBound - _cameraSystem.Zoom);
            _clampDiff = newPosition - clampPosition;
            _cameraSystem.SetCameraPosition(clampPosition);
            _startDragPos -= (Vector2)_clampDiff;
            _previousInputWorldPosition = _inputSystem.WorldPosition;
        }

        private void OnDragHandlerBeginDrag()
        {
            _clampDiff = Vector3.zero;
            _previousInputWorldPosition = _inputSystem.WorldPosition;
            _startCamPos = _cameraSystem.CameraPosition;
            _startDragPos = _inputSystem.WorldPosition;
            _canMove = true;
        }

        private void OnDragHandlerDrag()
        {
        }

        private void OnDragHandlerEndDrag()
        {
            _canMove = false;
        }

        public void OnLevelLoaded(Vector2 bottomLeftBound, Vector2 topRightBound)
        {
            var mapSizeInfo = new Rect
            {
                width = topRightBound.x - bottomLeftBound.x,
                height = topRightBound.y - bottomLeftBound.y
            };

            _maxXBound = mapSizeInfo.xMax = topRightBound.x;
            _minXBound = mapSizeInfo.xMin = bottomLeftBound.x;
            _maxYBound = mapSizeInfo.yMax = topRightBound.y;
            _minYBound = mapSizeInfo.yMin = bottomLeftBound.y;
            mapSizeInfo.center = (topRightBound + bottomLeftBound) / 2;

            _maxZoomAvailable = Mathf.Min(mapSizeInfo.width / _cameraSystem.ScaleFactor, mapSizeInfo.height);
            _maxZoomAvailable /= 2;

            var startPos = (Vector3)mapSizeInfo.center;
            startPos.z = _cameraSystem.CameraPosition.z;
            _cameraSystem.SetCameraPosition(startPos);
        }
    }
}