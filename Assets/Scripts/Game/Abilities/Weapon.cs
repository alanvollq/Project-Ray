using Game.Charges;
using Game.Systems.DamageSystem;
using Game.Towers;
using UnityEngine;

namespace Game.Abilities
{
    public class Weapon
    {
        private readonly RadiusHandler _radiusHandler;
        private readonly WeaponStats _stats;
        private Charge _chargePrefab;
        private float _attackLeftTime;
        private readonly IDamageDealer _damageDealer;
        private Transform _parent;

        public Weapon(WeaponAbility weaponAbility, RadiusHandler radiusHandler, IDamageDealer damageDealer, Transform parent)
        {
            _stats = weaponAbility.Stats;
            _radiusHandler = radiusHandler;
            _damageDealer = damageDealer;
            _parent = parent;
        }

        public void Update()
        {
            _attackLeftTime -= Time.deltaTime;
            if (_attackLeftTime > 0)
                return;

            _attackLeftTime = _stats.AttackInterval;

            if (_radiusHandler.HasTarget == false)
                return;

            var enemy = _radiusHandler.GetTarget();
            var charge = Object.Instantiate(_chargePrefab, _parent);
            charge.transform.position = _parent.position;
            var damage = new Damage
            {
                Owner = _damageDealer,
                Value = _stats.Damage
            };
            charge.SetDamage(damage);
            charge.MoveToTarget(enemy.transform);
        }
    }
}