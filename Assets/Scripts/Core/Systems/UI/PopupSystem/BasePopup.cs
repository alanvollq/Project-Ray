using Core.GameContext;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Systems.UI.PopupSystem
{
    public abstract class BasePopup : MonoBehaviour
    {
        [SerializeField] private GameObject _content;
        [SerializeField] private bool _openingIsSingle;
        [SerializeField] private bool _hidePrev;
        [SerializeField] private Button _backField;
        [SerializeField] private Button _closeButton;
        
        private bool _isShowed;
        
        protected IPopupSystem PopupSystem;
        protected IGameContext GameContext;


        public RectTransform RectTransform { get; private set; }
        public bool OpeningIsSingle => _openingIsSingle;
        public bool HidePrev => _hidePrev;
        public bool IsShowed => _isShowed;


        public BasePopup Initialization(IPopupSystem popupSystem, IGameContext gameContext)
        {
            PopupSystem = popupSystem;
            GameContext = gameContext;
            
            if (_backField != null)
                _backField.onClick.AddListener(PopupSystem.CloseLast);
            
            if (_closeButton != null)
                _closeButton.onClick.AddListener(PopupSystem.CloseLast);

            RectTransform = GetComponent<RectTransform>();
            
            OnInitialization();
            
            gameObject.SetActive(false);

            return this;
        }

        protected virtual void OnInitialization()
        {
        }

        public void Open(IUIElementOpenParam openParam)
        {
            OnBeforeOpen(openParam);
            gameObject.SetActive(true);
            OnAfterOpen(openParam);
            Show();
        }

        public void Close()
        {
            Hide();
            OnBeforeClose();
            gameObject.SetActive(false);
            OnAfterClose();
        }

        public void Show()
        {
            _content.SetActive(true);
            _isShowed = true;
        }

        public void Hide()
        {
            _content.SetActive(false);
            _isShowed = false;
        }

        protected virtual void OnBeforeOpen(IUIElementOpenParam openParam)
        {
        }

        protected virtual void OnAfterOpen(IUIElementOpenParam openParam)
        {
        }

        protected virtual void OnBeforeClose()
        {
        }

        protected virtual void OnAfterClose()
        {
        }

        protected virtual void OnBeforeShow()
        {
        }

        protected virtual void OnAfterShow()
        {
        }

        protected virtual void OnBeforeHide()
        {
        }

        protected virtual void OnAfterHide()
        {
        }
    }
}