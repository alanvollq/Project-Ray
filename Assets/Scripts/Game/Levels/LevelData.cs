using UnityEngine;

namespace Game.Levels
{
    [CreateAssetMenu(fileName = "Level Data", menuName = "Data/Game/Levels/Level Data")]
    public class LevelData : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField, TextArea] private string _description;
        [SerializeField] private LevelWavesData _levelWavesData;

        
        public string Name => _name;
        public string Description => _description;
        public LevelWavesData LevelWavesData => _levelWavesData;
    }
}