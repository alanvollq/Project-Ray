﻿using System.Collections.Generic;
using Core.GameContext;
using UnityEngine;

namespace Core.Systems.UpdateSystem
{
    public class UpdatableSystem : MonoBehaviour, IUpdateSystem, IGameContextInitializable
    {
        private readonly List<IUpdatable> _updatable = new();


        private void Update()
        {
            foreach (var updatable in _updatable)
                updatable.Update();
        }

        public void AddUpdatable(IUpdatable updatable)
        {
            _updatable.Add(updatable);
        }

        public void RemoveUpdatable(IUpdatable updatable)
        {
            _updatable.Remove(updatable);
        }

        public void Initialization(IGameContext gameContext)
        {
            transform.SetParent(gameContext.RootOwner.CoreRoot);
        }
    }
}