using System.Collections.Generic;
using Core.Loggers;
using UnityEngine;

namespace Core.Data
{
    [CreateAssetMenu(fileName = "SO Log Data", menuName = "Data/Core/Loggers/SO Log Data")]
    public class SOLogData : ScriptableObject, ILogData
    {
        [SerializeField] private SOLogData _parentLogData;
        [SerializeField] private bool _isEnable;
        [SerializeField] private ColoredText _prefix;
        [SerializeField] private Color _textColor = Color.white;


        public Color TextColor => _textColor;

        public bool IsEnable => (_parentLogData == null || _parentLogData.IsEnable) && _isEnable;

        
        public List<ColoredText> GetPrefixes()
        {
            var prefixes = new List<ColoredText>();
            if (_parentLogData != null)
                prefixes.AddRange(_parentLogData.GetPrefixes());

            prefixes.Add(_prefix);
            return prefixes;
        }
    }
}