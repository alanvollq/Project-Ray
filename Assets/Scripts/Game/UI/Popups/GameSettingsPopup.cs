using Core.Systems.UI.PopupSystem;
using Game.Systems.CameraSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public sealed class GameSettingsPopup : BasePopup
    {
        [SerializeField] private TMP_Text _cameraSpeedText;
        [SerializeField] private Button _increaseCameraSpeedButton;
        [SerializeField] private Button _increaseCameraSpeedLowButton;
        [SerializeField] private Button _decreaseCameraSpeedButton;
        [SerializeField] private Button _decreaseCameraSpeedLowButton;
        
        private ICameraSystem _cameraSystem;


        protected override void OnInitialization()
        {
            _cameraSystem = GameContext.GetSystem<ICameraSystem>();
            
            _increaseCameraSpeedButton.onClick.AddListener(OnIncreaseCameraSpeedButtonClick);
            _increaseCameraSpeedLowButton.onClick.AddListener(OnIncreaseCameraSpeedLowButtonClick);
            _decreaseCameraSpeedButton.onClick.AddListener(OnDecreaseCameraSpeedButtonClick);
            _decreaseCameraSpeedLowButton.onClick.AddListener(OnDecreaseCameraSpeedLowButtonClick);
        }

        private void OnIncreaseCameraSpeedButtonClick()
        {
            ChangeCameraSpeed(1);
        }

        private void OnIncreaseCameraSpeedLowButtonClick()
        {
            ChangeCameraSpeed(0.1f);
        }

        private void OnDecreaseCameraSpeedButtonClick()
        {
            ChangeCameraSpeed(-1);
        }

        private void OnDecreaseCameraSpeedLowButtonClick()
        {
            ChangeCameraSpeed(-0.1f);
        }

        private void ChangeCameraSpeed(float value)
        {
            _cameraSystem.SetMoveSpeed(_cameraSystem.MoveSpeed + value);
            _cameraSpeedText.text = $"{_cameraSystem.MoveSpeed}";
        }
    }
}