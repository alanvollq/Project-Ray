using System;

namespace Game.Systems.DamageSystem
{
    public interface IDamageDealer
    {
        public event Action<DamageResult> OnDealDamage;
        
        
        public void AddDamageResult(DamageResult damageResult);
    }
}