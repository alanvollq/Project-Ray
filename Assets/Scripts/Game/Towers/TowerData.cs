﻿using Game.Abilities;
using Game.CreatureTypes;
using UnityEngine;

namespace Game.Towers
{
    [CreateAssetMenu(fileName = "Tower Data", menuName = "Data/Game/Towers/Tower Data")]
    public class TowerData : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private Sprite _icon;
        [SerializeField] private string _description;
        [SerializeField] private AbilitySlots _abilities;
        [SerializeField] private CreatureType _creatureType;


        public string Name => _name;
        public Sprite Icon => _icon;
        public string Description => _description;
        public CreatureType CreatureType => _creatureType;
        public AbilitySlots Abilities => _abilities;
    }
}