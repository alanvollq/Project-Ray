using Game.Systems.BuildSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public sealed class UIBuildListItem : MonoBehaviour
    {
        [SerializeField] private Toggle _toggle;
        [SerializeField] private BuildItemData _buildItemData;

        private IBuildSystem _buildSystem;


        private void Awake()
        {
            _toggle.onValueChanged.AddListener(OnValueChanged);
        }

        public void Init(IBuildSystem buildSystem)
        {
            _buildSystem = buildSystem;
        }

        private void OnValueChanged(bool value)
        {
            if (value)
            {
                _buildSystem.PlacingCompleted += BuildSystemOnPlacingCompleted;
                _buildSystem.StartPlacing(_buildItemData);
            }
            else
            {
                _buildSystem.CancelPlacing();
                _buildSystem.PlacingCompleted -= BuildSystemOnPlacingCompleted;
            }
        }

        private void BuildSystemOnPlacingCompleted()
        {
            _toggle.isOn = false;
        }
    }
}