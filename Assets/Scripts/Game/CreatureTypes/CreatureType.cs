using UnityEngine;

namespace Game.CreatureTypes
{
    [CreateAssetMenu(fileName = "Types Of Creatures", menuName = "Data/Game/Types Of Creatures")]
    public class CreatureType : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private Sprite _icon;
        [SerializeField] private CreatureType _advantage;
        [SerializeField] private CreatureType _disadvantage;


        public string Name => _name;
        public Sprite Icon => _icon;
        public CreatureType Advantage => _advantage;
        public CreatureType Disadvantage => _disadvantage;
    }
}