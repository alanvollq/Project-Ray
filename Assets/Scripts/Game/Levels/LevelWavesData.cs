using System.Collections.Generic;
using System.Linq;
using Game.Enemies;
using UnityEngine;

namespace Game.Levels
{
    [CreateAssetMenu(fileName = "Level Waves Data", menuName = "Data/Game/Levels/Level Waves Data")]
    public class LevelWavesData : ScriptableObject
    {
        [SerializeField] private List<WaveData> _waves;


        public List<WaveData> Waves => _waves;


        public IEnumerable<EnemyData> GetEnemies()
        {
            return _waves
                .SelectMany(wave => wave.WaveSpawnersData)
                .SelectMany(spawnerData => spawnerData.WavePartsData)
                .Select(wavePartData => wavePartData.EnemyData)
                .Distinct()
                .OrderBy(enemyData => enemyData.QualityType.Level)
                .ThenBy(enemyData => enemyData.EnemyType.name);
        }
    }
}