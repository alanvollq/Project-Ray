﻿using UnityEngine;

namespace Game.Systems.BuildSystem
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class PlacingItemPart : MonoBehaviour
    {
        [SerializeField] private Color _normalColor;
        [SerializeField] private Color _errorColor;
        
        private SpriteRenderer _spriteRenderer;
        private Collider2D _collider;
        private int _colliderCount;


        public bool HasCollision => _colliderCount > 0;


        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _collider = GetComponent<Collider2D>();
            
            _spriteRenderer.color = _normalColor;
            _collider.enabled = false;
        }

        private void OnEnable()
        {
            _collider.enabled = true;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            _colliderCount++;
            _spriteRenderer.color = _errorColor;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            _colliderCount--;
            _spriteRenderer.color = _colliderCount == 0 ? _normalColor : _errorColor;
        }
    }
}