using System;
using UnityEngine;

namespace Game.Abilities
{
    [Serializable]
    public class AbilitySlots
    {
        [SerializeField] private AbilitySlot<WeaponAbility> _weaponAbilitySlot;
        [SerializeField] private AbilitySlot<Ability> _passiveAbilitySlot;
        [SerializeField] private AbilitySlot<Ability> _activeAbilitySlot;


        public AbilitySlot<WeaponAbility> WeaponAbilitySlot => _weaponAbilitySlot;
        public AbilitySlot<Ability> PassiveAbilitySlot => _passiveAbilitySlot;
        public AbilitySlot<Ability> ActiveAbilitySlot => _activeAbilitySlot;


        public void Init()
        {
            _weaponAbilitySlot.SwitchAbility();
            _passiveAbilitySlot.SwitchAbility();
            _activeAbilitySlot.SwitchAbility();
        }
    }
}