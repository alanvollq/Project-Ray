﻿using System;
using UnityEngine;

namespace Core.Systems.TimeSystem
{
    public class TimeSystem : ITimeSystem
    {
        public event Action Paused;
        public event Action<float> UnPaused;

        
        private bool _isPause;
        private float _pauseTime;

        
        public bool IsPause => _isPause;
        
        
        public void Pause()
        {
            _isPause = true;
            _pauseTime = Time.time;
            Paused?.Invoke();
        }

        public void UnPause()
        {
            _isPause = false;
            UnPaused?.Invoke(Time.time - _pauseTime);
        }
    }
}