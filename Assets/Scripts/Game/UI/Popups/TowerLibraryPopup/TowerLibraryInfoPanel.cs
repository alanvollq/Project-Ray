using System.Linq;
using Game.Inventory;
using Game.Towers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.TowerLibraryPopup
{
    public class TowerLibraryInfoPanel : MonoBehaviour
    {
        [SerializeField] private TMP_Text _towerName;
        [SerializeField] private Image _towerIcon;
        [SerializeField] private Image _qualityBorder;
        [SerializeField] private TMP_Text _qualityName;
        [SerializeField] private TMP_Text _countText;
        [SerializeField] private TMP_Text _typeText;
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private Image _typeIcon;
        [SerializeField] private Image _countField;


        public void SetInfo(TowerInventoryItem towerInventoryItem)
        {
            _countText.text = $"x{towerInventoryItem.Count}";
            
            var qualityType = towerInventoryItem.QualityType;
            _qualityBorder.color = qualityType.Color;
            _qualityName.color = qualityType.Color;
            _countField.color = qualityType.Color;
            _qualityName.text = qualityType.Name;

            var towerData = towerInventoryItem.TowerData;
            _towerName.text = towerData.Name;
            _towerIcon.sprite = towerData.Icon;
            _descriptionText.text = towerData.Description;
            _typeText.text = towerData.CreatureType.Name;
            _typeIcon.sprite = towerData.CreatureType.Icon;
        }

        public void SetInfo(IGrouping<TowerData, TowerInventoryItem> towerInventoryItems)
        {
        }
    }
}