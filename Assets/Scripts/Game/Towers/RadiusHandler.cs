using System.Collections.Generic;
using Core.Loggers;
using Game.Enemies;
using UnityEngine;
using static Game.Data.Loggers.GameLogData;

namespace Game.Towers
{
    public class RadiusHandler : MonoBehaviour
    {
        [SerializeField] private List<Enemy> _enemies;


        public bool HasTarget => _enemies.Count > 0;

        public Enemy GetTarget()
        {
            return _enemies[0];
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var enemy = other.GetComponent<Enemy>();
            if (enemy == null)
                return;

            enemy.Released += EnemyOnReleased;
            _enemies.Add(enemy);
            TargetHandlerLogData.Log($"{name} - On trigger enter: {other.name}");
        }

        private void EnemyOnReleased(Enemy enemy)
        {
            _enemies.Remove(enemy);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var enemy = other.GetComponent<Enemy>();
            if (enemy == null)
                return;

            _enemies.Remove(enemy);
            TargetHandlerLogData.Log($"{name} - On trigger exit: {other.name}");
        }
    }
}