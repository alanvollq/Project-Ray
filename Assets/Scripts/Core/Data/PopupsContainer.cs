using Core.Systems.UI.PopupSystem;
using UnityEngine;

namespace Core.Data
{
    [CreateAssetMenu(fileName = "Popups Container", menuName = "Data/Core/Managers/UI/Popups Container")]
    public class PopupsContainer : DataContainer<BasePopup> { }
}