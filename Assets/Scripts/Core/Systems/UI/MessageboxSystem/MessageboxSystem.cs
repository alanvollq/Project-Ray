using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.GameContext;
using Core.Loggers;
using UnityEngine;
using static UnityEngine.Object;
using static Core.Loggers.CoreLogData;

namespace Core.Systems.UI.MessageboxSystem
{
    public sealed class MessageboxSystem : IMessageboxSystem, IGameContextInitializable
    {
        private readonly Dictionary<Type, BaseMessagebox> _messageBoxes;


        public MessageboxSystem(IEnumerable<BaseMessagebox> messageBoxes)
        {
            MessageboxSystemLogData.Log("Create");
            _messageBoxes = messageBoxes
                .Select(Instantiate)
                .ToDictionary(basePage => basePage.GetType());
        }


        public void Initialization(IGameContext gameContext)
        {
            MessageboxSystemLogData.Log("Initialize");
            var parentObject = new GameObject("[ MessageBoxes ]");
            var parentTransform = parentObject.AddComponent<RectTransform>();
            parentTransform.SetParent(gameContext.RootOwner.UIRoot.transform);
            parentTransform.SetDefaultMaxStretch();

            foreach (var messageBox in _messageBoxes.Values)
            {
                messageBox.transform.SetParent(parentTransform);
                messageBox
                    .Initialization(this, gameContext)
                    .RectTransform.SetDefaultMaxStretch();
            }
        }

        public void Open<TMessageBox>(Action okAction, Action cancelAction) where TMessageBox : BaseMessagebox
        {
        }
    }
}