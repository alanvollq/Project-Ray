using Core.GameContext;
using Core.GameLaunch;
using Core.Systems.UI.PageSystem;
using Game.UI.Pages;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Start Game", fileName = "Start Game Launch Task")]
    public class StartGameLaunchTask : LaunchTask
    {
        public override void Execute(IGameContext gameContext)
        {
            var pageSystem = gameContext.GetSystem<IPageSystem>();
            pageSystem.Open<UIMainPage>();
        }
    }
}