using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.GameContext;
using Core.Loggers;
using UnityEngine;
using static Core.Loggers.CoreLogData;
using static UnityEngine.Object;

namespace Core.Systems.UI.PopupSystem
{
    public sealed class PopupSystem : IPopupSystem, IGameContextInitializable
    {
        public event Action<BasePopup> PopupOpened;
        public event Action<BasePopup> PopupClosed;

        private readonly Dictionary<Type, BasePopup> _popups;
        private readonly Stack<BasePopup> _openedPopups = new();


        public PopupSystem(IEnumerable<BasePopup> basePopups)
        {
            PopupSystemLogData.Log("Create");
            _popups = basePopups
                .Select(Instantiate)
                .ToDictionary(basePage => basePage.GetType());
        }


        public void Initialization(IGameContext gameContext)
        {
            PopupSystemLogData.Log("Initialize");
            var parentObject = new GameObject("[ Popups ]");
            var parentTransform = parentObject.AddComponent<RectTransform>();
            parentTransform.SetParent(gameContext.RootOwner.UIRoot.transform);
            parentTransform.SetDefaultMaxStretch();
            
            foreach (var popup in _popups.Values)
            {
                popup.transform.SetParent(parentTransform);
                popup
                    .Initialization(this, gameContext)
                    .RectTransform.SetDefaultMaxStretch();
            }
        }

        public void Open<TPopup>(IUIElementOpenParam openParam = null) where TPopup : BasePopup
        {
            var key = typeof(TPopup);
            _popups.TryGetValue(key, out var openingPopup);
            if (openingPopup == null)
                throw new Exception($"Popup [{key}] is not found!!!");

            if (openingPopup.OpeningIsSingle && _openedPopups.Count > 0)
                CloseAll();

            if (openingPopup.HidePrev && _openedPopups.Count > 0)
                _openedPopups.Peek().Hide();

            openingPopup.Open(openParam);
            _openedPopups.Push(openingPopup);

            PopupOpened?.Invoke(openingPopup);
        }

        public void CloseLast()
        {
            if (_openedPopups.Count < 1)
                return;

            CloseTop();

            if (_openedPopups.Count < 1)
                return;

            if (_openedPopups.Peek().IsShowed)
                return;

            _openedPopups.Peek().Show();
        }

        private void CloseTop()
        {
            if (_openedPopups.Count < 1)
                return;

            var closingPopup = _openedPopups.Pop();
            closingPopup.Close();
            PopupClosed?.Invoke(closingPopup);
        }

        public void CloseAll()
        {
            while (_openedPopups.Count > 0)
                CloseTop();
        }
    }
}