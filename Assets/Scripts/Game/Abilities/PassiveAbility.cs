using UnityEngine;

namespace Game.Abilities
{
    [CreateAssetMenu(fileName = "Passive Ability", menuName = "Data/Game/Abilities/Passive Ability")]
    public class PassiveAbility : Ability
    {
        
    }
}