using System.Collections.Generic;
using Core.Container;
using Core.Root;
using Core.Managers;
using Core.Systems;

namespace Core.GameContext
{
    public class GameContext : IGameContext
    {
        private readonly IContainer<ISystem> _systemContainer = new Container<ISystem>();
        private readonly IContainer<IManager> _managerContainer = new Container<IManager>();
        private readonly List<IGameContextInitializable> _initializableItems = new();


        public GameContext(IRootOwner rootOwner)
        {
            RootOwner = rootOwner;
        }


        public IRootOwner RootOwner { get; }


        public IGameContext AddSystem<TSystem>(TSystem system) where TSystem : ISystem
        {
            _systemContainer.Add(system);
            AddInitializableItem(system);

            return this;
        }

        public IGameContext AddManager<TManager>(TManager manager) where TManager : IManager
        {
            _managerContainer.Add(manager);
            AddInitializableItem(manager);

            return this;
        }

        public TSystem GetSystem<TSystem>() where TSystem : ISystem
        {
            return _systemContainer.Get<TSystem>();
        }

        public TManager GetManager<TManager>() where TManager : IManager
        {
            return _managerContainer.Get<TManager>();
        }

        private void AddInitializableItem(object item)
        {
            if (item is IGameContextInitializable gameContextInitializable)
                _initializableItems.Add(gameContextInitializable);
        }

        public void InitializeItems()
        {
            _initializableItems.ForEach(initializable => initializable.Initialization(this));
        }
    }
}