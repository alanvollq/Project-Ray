using Core.Managers;
using Core.Root;
using Core.Systems;

namespace Core.GameContext
{
    public interface IGameContext
    {
        public IRootOwner RootOwner { get; }


        public void InitializeItems();
        IGameContext AddSystem<TSystem>(TSystem system) where TSystem : ISystem;
        public TSystem GetSystem<TSystem>() where TSystem : ISystem;
        public IGameContext AddManager<TManager>(TManager manager) where TManager : IManager;
        public TManager GetManager<TManager>() where TManager : IManager;
    }
}