﻿using System;
using Core.Systems;
using UnityEngine;

namespace Game.Systems.InputSystem
{
    public interface IInputSystem : ISystem
    {
        public event Action<float> OnZoomStarted;
        public Vector2 DeltaMove { get; }

        public Vector2 WorldPosition { get; }

        public Vector2 ScreenPosition { get; }
        public Vector2 WorldTouchPosition { get; }

        public void Enable();
        public void Disable();
    }
}