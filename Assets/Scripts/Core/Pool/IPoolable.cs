namespace Core.Pool
{
    public interface IPoolable
    {
        public void Recycle();
        public void Release();
    }
}