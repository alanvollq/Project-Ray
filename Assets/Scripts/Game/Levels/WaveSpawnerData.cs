using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Levels
{
    [Serializable]
    public class WaveSpawnerData
    {
        [SerializeField] private List<WavePartData> _wavePartsData;


        public IEnumerable<WavePartData> WavePartsData => _wavePartsData;
    }
}