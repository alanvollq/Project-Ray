using System.Linq;
using Core.Extensions;
using UnityEngine;

namespace Core.Loggers
{
    public static class ConsoleLogger
    {
        public static void Log(this ILogData logData, string text)
        {
            if (ReferenceEquals(logData, null))
                return;

            if (logData.IsEnable == false)
                return;

            var prefix = logData.GetPrefixes().Aggregate("",
                (current, logDataPrefix) => current + GetColoredPrefix(logDataPrefix));

            prefix = string.IsNullOrEmpty(prefix) ? string.Empty : prefix + ": ";

            var message = text.Color(logData.TextColor);

            Debug.Log($"{prefix}{message}");
        }

        public static void Log(string value, ILogData logData) => logData.Log(value);
        public static void Log(string value) => Debug.Log(value);

        private static string GetColoredPrefix(ColoredText coloredText)
        {
            if (string.IsNullOrEmpty(coloredText.Text))
                return string.Empty;
            
            var coloredPrefix = $"[ {coloredText.Text} ]"
                .Color(coloredText.Color)
                .Bold();

            return coloredPrefix;
        }
    }
}