using UnityEngine;

namespace Game.Inventory
{
    public abstract class InventoryItem : ScriptableObject
    {
        [SerializeField] private int _count;


        public int Count => _count;


        public void SetCount(int value)
        {
            _count = value;
        }
    }
}