using System;
using Game.Enemies;
using UnityEngine;

namespace Game.Levels
{
    [Serializable]
    public class WavePartData
    {
        [SerializeField] private EnemyData _enemyData;
        [SerializeField] private int _count;
        [SerializeField] private float _interval;
        [SerializeField] private float _startDelay;
        [SerializeField] private int _spawnerNumber;


        public EnemyData EnemyData => _enemyData;
        public int Count => _count;
        public float Interval => _interval;
        public float StartDelay => _startDelay;
        public int SpawnerNumber => _spawnerNumber;
    }
}