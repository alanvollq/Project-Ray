using Core.GameContext;
using UnityEngine;

namespace Core.GameLaunch
{
    public abstract class LaunchTask : ScriptableObject
    {
        public abstract void Execute(IGameContext gameContext);
    }
}