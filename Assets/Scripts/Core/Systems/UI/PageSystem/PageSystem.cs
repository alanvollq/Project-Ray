using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.GameContext;
using Core.Loggers;
using UnityEngine;
using static UnityEngine.Object;
using static Core.Loggers.CoreLogData;

namespace Core.Systems.UI.PageSystem
{
    public sealed class PageSystem : IPageSystem, IGameContextInitializable
    {
        public event Action PageOpened;
        public event Action PageClosed;


        private readonly IDictionary<Type, BasePage> _pages;
        private BasePage _openedPage;


        public PageSystem(IEnumerable<BasePage> basePages)
        {
            PageSystemLogData.Log("Create");
            _pages = basePages
                .Select(Instantiate)
                .ToDictionary(basePage => basePage.GetType());
        }


        public void Initialization(IGameContext gameContext)
        {
            PageSystemLogData.Log("Initialize");
            var parentObject = new GameObject("[ Pages ]");
            var parentTransform = parentObject.AddComponent<RectTransform>();
            
            parentTransform.SetParent(gameContext.RootOwner.UIRoot);
            parentTransform.SetDefaultMaxStretch();

            foreach (var page in _pages.Values)
            {
                page.transform.SetParent(parentTransform);
                page.Initialization(this, gameContext)
                    .RectTransform.SetDefaultMaxStretch();
            }
        }

        public void Open<TPage>() where TPage : BasePage
        {
            var type = typeof(TPage);
            if (!_pages.TryGetValue(type, out var openingPage))
                return;

            CloseLast();
            openingPage.Open();
            _openedPage = openingPage;

            PageOpened?.Invoke();
        }

        private void CloseLast()
        {
            if (_openedPage is null)
                return;

            _openedPage.Close();
            PageClosed?.Invoke();
        }
    }
}