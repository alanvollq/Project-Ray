using Core.Systems.UI.PageSystem;
using UnityEngine;

namespace Core.Data
{
    [CreateAssetMenu(fileName = "Pages Container", menuName = "Data/Core/Managers/UI/Pages Container")]
    public class PagesContainer : DataContainer<BasePage> { }
}