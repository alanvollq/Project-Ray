using Core.Managers;

namespace Game.Towers
{
    public interface ITowerManager : IManager
    {
        public void Select(TowerController towerController);
    }
}