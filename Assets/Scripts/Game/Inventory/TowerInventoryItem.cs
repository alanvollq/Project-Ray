using Game.QualityTypes;
using Game.Towers;
using UnityEngine;

namespace Game.Inventory
{
    [CreateAssetMenu(fileName = "Tower Inventory Item", menuName = "Data/Game/Inventory/Tower Inventory Item")]
    public class TowerInventoryItem : InventoryItem
    {
        [SerializeField] private TowerData _towerData;
        [SerializeField] private QualityType _qualityType;


        public TowerData TowerData => _towerData;
        public QualityType QualityType => _qualityType;


        [ContextMenu("SetRandomCount")]
        private void SetRandomCount()
        {
            var maxValue = _qualityType.Level switch
            {
                0 => 25,
                1 => 21,
                2 => 16,
                3 => 11,
                4 => 6,
                5 => 3,
                _ => 0
            };

            var count = Random.Range(-10, maxValue);
            count = Mathf.Clamp(count, 0, 20);

            SetCount(count);
        }
    }
}