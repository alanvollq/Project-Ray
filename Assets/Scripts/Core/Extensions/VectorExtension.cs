using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Extensions
{
    public static class VectorExtension
    {
        public static float Distance(this IEnumerable<Vector2> path)
        {
            var replacedPath = path.Select(vector2 => new Vector3(vector2.x, vector2.y));

            return Distance(replacedPath);
        }

        public static float Distance(this IEnumerable<Vector3>path)
        {
            var distance = 0f;
            var enumerable = path as Vector3[] ?? path.ToArray();
            for (var i = 0; i < enumerable.Length - 1; i++)
                distance += Vector3.Distance(enumerable[i], enumerable[i + 1]);

            return distance;
        }
    }
}