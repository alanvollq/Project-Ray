using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UIElements
{
    [RequireComponent(typeof(Button))]
    public class UIToggle : MonoBehaviour
    {
        public event Action<bool> OnValueChanged;

        [SerializeField] private UIToggleGroup _toggleGroup;
        [SerializeField] private GameObject _checkMark;

        private Button _button;
        private bool _isOn;


        public bool IsOn => _isOn;


        private void Awake()
        {
            if (_toggleGroup != null)
                _toggleGroup.AddToggle(this);
            
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnButtonDown);
        }

        private void OnButtonDown()
        {
            SetValue(!_isOn);
        }

        public void SetValue(bool value)
        {
            _isOn = value;
            _checkMark.SetActive(value);
            OnValueChanged?.Invoke(_isOn);

            if (value && _toggleGroup != null)
                _toggleGroup.SetActiveToggle(this);
        }
    }
}