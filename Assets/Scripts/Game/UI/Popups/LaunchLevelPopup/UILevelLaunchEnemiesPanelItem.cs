using Game.Enemies;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups.LaunchLevelPopup
{
    public class LevelLaunchEnemiesPanelItem : MonoBehaviour
    {
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private Image _enemyIcon;
        [SerializeField] private Image _typeIcon;
        [SerializeField] private Image _border;
        
        
        public void SetInfo(EnemyData enemyData)
        {
            _nameText.text = enemyData.EnemyType.Name;
            _enemyIcon.sprite = enemyData.Icon;
            _typeIcon.sprite = enemyData.EnemyType.CreatureType.Icon;
            _border.color = enemyData.QualityType.Color;
        }
    }
}