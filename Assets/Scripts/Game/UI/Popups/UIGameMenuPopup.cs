using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Game.UI.Pages;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Popups
{
    public sealed class UIGameMenuPopup : BasePopup
    {
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _settingsButton;

        private IPageSystem _pageSystem;


        protected override void OnInitialization()
        {
            _pageSystem = GameContext.GetSystem<IPageSystem>();

            _exitButton.onClick.AddListener(OnExitButtonClick);
            _settingsButton.onClick.AddListener(OnSettingsButtonClick);
        }

        private void OnExitButtonClick()
        {
            _pageSystem.Open<UIMainPage>();
        }

        private void OnSettingsButtonClick()
        {
            PopupSystem.Open<GameSettingsPopup>();
        }
    }
}