using System;

namespace Core.Systems.UI.MessageboxSystem
{
    public interface IMessageboxSystem : ISystem
    {
        public void Open<TMessageBox>(Action okAction, Action cancelAction) where TMessageBox : BaseMessagebox;
    }
}