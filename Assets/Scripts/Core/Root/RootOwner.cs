using UnityEngine;

namespace Core.Root
{
    public sealed class RootOwner : MonoBehaviour, IRootOwner
    {
        [SerializeField] private Transform _parentRoot;
        [SerializeField] private Transform _gameRoot;
        [SerializeField] private Transform _coreRoot;
        [SerializeField] private RectTransform _uiRoot;


        public Transform ParentRoot => _parentRoot;
        public Transform GameRoot => _gameRoot;
        public Transform CoreRoot => _coreRoot;
        public RectTransform UIRoot => _uiRoot;
    }
}