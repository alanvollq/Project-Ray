using System;
using Core.Systems;

namespace Game.Systems.BuildSystem
{
    public interface IBuildSystem : ISystem
    {
        public event Action PlacingStarted;
        public event Action PlacingCanceled;
        public event Action PlacingCompleted;
        
        
        public void StartPlacing(BuildItemData buildItemData);
        public void AcceptPlacing();
        public void CancelPlacing();
        public void RotatePlacingItem();
        public void DestroyAllBuilds();
    }
}