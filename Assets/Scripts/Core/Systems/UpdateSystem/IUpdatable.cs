﻿namespace Core.Systems.UpdateSystem
{
    public interface IUpdatable
    {
        public void Update();
    }
}