﻿using System;
using UnityEngine;

namespace Game.Systems.CameraSystem
{
    public class CameraSystem : ICameraSystem
    {
        public event Action<float> ZoomChanged;

        private readonly Transform _cameraTransform;
        private readonly Camera _mainCamera;
        private float _moveSpeed = 3;
        private float _ratio;

        
        private const float MinMoveSpeed = 0.1f;


        public Camera MainCamera => _mainCamera;

        public Vector3 CameraPosition => _cameraTransform.position;
        
        public float MoveSpeed => _moveSpeed;
        
        public float Zoom => _mainCamera.orthographicSize;
        
        public float ScaleFactor => (float)_mainCamera.pixelWidth / _mainCamera.pixelHeight;
        
        public float OrthographicSize => _mainCamera.orthographicSize;


        public CameraSystem(Camera mainCamera)
        {
            _mainCamera = mainCamera;
            _cameraTransform = mainCamera.transform;
        }

        public void SetCameraPosition(Vector3 position)
        {
            _cameraTransform.position = position;
        }

        public void SetOrthographicSize(float value)
        {
            _mainCamera.orthographicSize = value;
            ZoomChanged?.Invoke(value);
        }

        public void Move(Vector2 deltaMove)
        {
            _cameraTransform.position -= new Vector3(deltaMove.x, deltaMove.y, 0) * (Time.deltaTime * _moveSpeed);
        }

        public void SetMoveSpeed(float value)
        {
            if (value < MinMoveSpeed)
                value = MinMoveSpeed;

            _moveSpeed = value;
        }
    }
}