using Game.UI;
using UnityEngine;

namespace Game.Enemies
{
    public class EnemyView : MonoBehaviour
    {
        [SerializeField] private TransformFillBar _fillBar;
        
        private EnemyStats _stats;
        

        private void OnHealthChanged()
        {
            _fillBar.SetValue(_stats.PercentCurrentValue);
        }

        public void Init(EnemyStats stats)
        {
            _stats = stats;
            OnHealthChanged();
            _stats.HealthChanged += OnHealthChanged;
        }
    }
}