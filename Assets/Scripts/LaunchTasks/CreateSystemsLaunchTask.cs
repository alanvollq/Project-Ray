using Core.GameContext;
using Core.GameLaunch;
using Core.Systems.TimeSystem;
using Core.Systems.UI.MessageboxSystem;
using Core.Systems.UI.PageSystem;
using Core.Systems.UI.PopupSystem;
using Core.Systems.UpdateSystem;
using Game.Setup;
using Game.Systems.BuildSystem;
using Game.Systems.CameraSystem;
using Game.Systems.InputSystem;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Systems", fileName = "Create Systems Launch Task")]
    public sealed class CreateSystemsLaunchTask : LaunchTask
    {
        [SerializeField] private SystemsData _systemsData;


        public override void Execute(IGameContext gameContext)
        {
            CreateCoreSystems(gameContext);
            CreateGameSystems(gameContext);
        }

        private void CreateCoreSystems(IGameContext gameContext)
        {
            var timeSystem = new TimeSystem();
            var updatableSystem = new GameObject("Updatable Manager").AddComponent<UpdatableSystem>();

            gameContext.AddSystem<ITimeSystem>(timeSystem);
            gameContext.AddSystem<IUpdateSystem>(updatableSystem);

            CreateUISystems(gameContext);
        }

        private void CreateUISystems(IGameContext gameContext)
        {
            var uiContainer = _systemsData.UIContainer;
            var pageSystem = new PageSystem(uiContainer.Pages);
            var popupSystem = new PopupSystem(uiContainer.Popups);
            var messageboxSystem = new MessageboxSystem(uiContainer.MessageBoxes);

            gameContext.AddSystem<IPageSystem>(pageSystem);
            gameContext.AddSystem<IPopupSystem>(popupSystem);
            gameContext.AddSystem<IMessageboxSystem>(messageboxSystem);
        }

        private void CreateGameSystems(IGameContext gameContext)
        {
            var buildSystem = new BuildSystem();
            var inputSystem = new InputSystem();
            var cameraSystem = new CameraSystem(Camera.main);

            gameContext.AddSystem<IBuildSystem>(buildSystem);
            gameContext.AddSystem<IInputSystem>(inputSystem);
            gameContext.AddSystem<ICameraSystem>(cameraSystem);
        }
    }
}