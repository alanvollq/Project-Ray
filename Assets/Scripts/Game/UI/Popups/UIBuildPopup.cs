using System.Collections.Generic;
using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Game.Inventory;
using Game.Systems.BuildSystem;
using UnityEngine;
using UnityEngine.UI;
using static Core.Loggers.ConsoleLogger;
using static Game.Data.Loggers.GameLogData;

namespace Game.UI.Popups
{
    public sealed class UIBuildPopup : BasePopup
    {
        [SerializeField] private Button _acceptButton;
        [SerializeField] private Button _rotateButton;
        [SerializeField] private List<TowerInventoryItem> _towerInventoryItems;
        [SerializeField] private List<UIBuildListItem> _buildListItems;

        private IBuildSystem _buildSystem;


        protected override void OnInitialization()
        {
            _buildSystem = GameContext.GetSystem<IBuildSystem>();

            _buildSystem.PlacingStarted += BuildSystemOnPlacingStarted;
            _buildSystem.PlacingCanceled += BuildSystemOnPlacingCanceled;

            _acceptButton.onClick.AddListener(OnAcceptButtonClick);
            _rotateButton.onClick.AddListener(OnRotateButtonClick);

            _buildListItems.ForEach(item => item.Init(_buildSystem));
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            _acceptButton.interactable = false;
        }

        private void BuildSystemOnPlacingCanceled()
        {
            _acceptButton.interactable = false;
        }

        private void BuildSystemOnPlacingStarted()
        {
            _acceptButton.interactable = true;
        }

        private void OnAcceptButtonClick()
        {
            Log("Build Popup: accept button click", UILogData.Popup);
            _buildSystem.AcceptPlacing();
        }

        private void OnRotateButtonClick()
        {
            Log("Build Popup: rotate button click", UILogData.Popup);
            _buildSystem.RotatePlacingItem();
        }

        protected override void OnBeforeClose()
        {
            _buildSystem.CancelPlacing();
        }
    }
}


public class UIBuildPopupOpenParam : IUIElementOpenParam
{
    public UIBuildPopupOpenParam()
    {
    }
}