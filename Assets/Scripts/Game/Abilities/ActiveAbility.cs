using UnityEngine;

namespace Game.Abilities
{
    [CreateAssetMenu(fileName = "Active Ability", menuName = "Data/Game/Abilities/Active Ability")]
    public class ActiveAbility : Ability
    {
        
    }
}