using Core.GameContext;
using Core.GameLaunch;
using Core.Loggers;
using Game.Data.Loggers;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Init Log Data", fileName = "Init Log Data Launch Task")]
    public class InitLogDataLaunchTask : LaunchTask
    {
        [SerializeField] private CoreLogData _coreLogData;
        [SerializeField] private GameLogData _gameLogData;

        public override void Execute(IGameContext gameContext)
        {
            _coreLogData.Init();
            _gameLogData.Init();
        }
    }
}