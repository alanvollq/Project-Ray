﻿using Core.Loggers;
using Core.Systems.UI;
using Core.Systems.UI.PopupSystem;
using Core.UIElements;
using Game.Towers;
using Game.UI.Popups.TowerPopup;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Game.Data.Loggers.GameLogData;

namespace Game.UI.Popups
{
    public class UITowerPopup : BasePopup
    {
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private UITowerStatsPanel _statsPanel;
        [SerializeField] private UITowerStatisticPanel _statisticPanel;
        [SerializeField] private UITowerAbilityPanel _abilityPanel;
        [SerializeField] private UIPanel _upgradePanel;
        [SerializeField] private UIToggle _infoButton;
        [SerializeField] private UIToggle _statisticButton;
        [SerializeField] private UIToggle _upgradeButton;
        [SerializeField] private UIToggle _abilityButton;
        [SerializeField] private Image _typeIcon;
        [SerializeField] private TMP_Text _typeLabel;
        [SerializeField] private Image _towerIcon;

        private TowerController _tower;
        private UIToggle _lastOpened;


        protected override void OnInitialization()
        {
            _infoButton.OnValueChanged += OnInfoButtonDown;
            _statisticButton.OnValueChanged += OnStatisticButtonDown;
            _upgradeButton.OnValueChanged += OnUpgradeButtonDown;
            _abilityButton.OnValueChanged += OnAbilityButtonDown;
        }

        protected override void OnBeforeOpen(IUIElementOpenParam openParam)
        {
            if (openParam is not UITowerPopupOpenParam param)
                return;

            _tower = param.Tower;

            _nameText.text = $"{_tower.name}";
            _statsPanel.SetInfo(_tower.WeaponStats);
            _statisticPanel.SetTower(_tower);
            _abilityPanel.SetTower(_tower);
            _typeIcon.sprite = _tower.Type.Icon;
            _typeLabel.text = _tower.Type.name;
            _towerIcon.sprite = _tower.Icon;
            _tower.Selected();
        }

        protected override void OnAfterOpen(IUIElementOpenParam openParam)
        {
            var toggle = _lastOpened == null ? _infoButton : _lastOpened;
            UILogData.Popup.Log($"OnAfterOpen toggle: {toggle.name}");
            toggle.SetValue(true);
        }

        protected override void OnBeforeClose()
        {
            _tower.Deselected();
        }

        private void OnInfoButtonDown(bool value)
        {
            UILogData.Popup.Log($"OnInfoButtonDown: {value}");
            _statsPanel.SetEnable(value);
            
            if (value)
                _lastOpened = _infoButton;
        }

        private void OnStatisticButtonDown(bool value)
        {
            UILogData.Popup.Log($"OnStatisticButtonDown: {value}");
            _statisticPanel.SetEnable(value);
            
            if (value)
                _lastOpened = _statisticButton;
        }

        private void OnUpgradeButtonDown(bool value)
        {
            UILogData.Popup.Log($"OnUpgradeButtonDown: {value}");
            _upgradePanel.SetEnable(value);
            
            if (value)
                _lastOpened = _upgradeButton;
        }

        private void OnAbilityButtonDown(bool value)
        {
            UILogData.Popup.Log($"OnAbilityButtonDown: {value}");
            _abilityPanel.SetEnable(value);
            
            if (value)
                _lastOpened = _abilityButton;
        }
    }

    public class UITowerPopupOpenParam : IUIElementOpenParam
    {
        public UITowerPopupOpenParam(TowerController tower)
        {
            Tower = tower;
        }

        public TowerController Tower { get; }
    }
}