using Core.GameContext;
using UnityEngine;

namespace Core.Systems.UI.MessageboxSystem
{
    public abstract class BaseMessagebox : MonoBehaviour
    {
        protected IMessageboxSystem MessageboxSystem;
        protected IGameContext GameContext;

        
        public RectTransform RectTransform { get; private set; }
        

        public BaseMessagebox Initialization(IMessageboxSystem messageboxSystem, IGameContext gameContext)
        {
            MessageboxSystem = messageboxSystem;
            GameContext = gameContext;

            RectTransform = GetComponent<RectTransform>();
            
            OnInitialization();
            
            gameObject.SetActive(false);

            return this;
        }

        protected virtual void OnInitialization()
        {
        }
    }
}