using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.GameContext;
using Game.Systems.CameraSystem;
using Game.Systems.InputSystem;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.Systems.BuildSystem
{
    public class BuildSystem : IBuildSystem, IGameContextInitializable
    {
        public event Action PlacingStarted;
        public event Action PlacingCanceled;
        public event Action PlacingCompleted;

        private Transform _buildsParent;
        private Transform _placingItemParent;
        private BuildItemData _buildItemData;
        private PlacingItem _placingItem;
        private IInputSystem _inputSystem;
        private ICameraSystem _cameraSystem;
        
        private readonly List<Build> _builds = new ();
        private IGameContext _gameContext;


        public void Initialization(IGameContext gameContext)
        {
            _gameContext = gameContext;
            _inputSystem = _gameContext.GetSystem<IInputSystem>();
            _cameraSystem = _gameContext.GetSystem<ICameraSystem>();

            _buildsParent = new GameObject("[ Builds ]").transform;
            _placingItemParent = new GameObject("[ Placing Item ]").transform;

            _placingItemParent.SetParent(_buildsParent);
            _buildsParent.SetParent(_gameContext.RootOwner.GameRoot);
        }

        public void StartPlacing(BuildItemData buildItemData)
        {
            if (_placingItem != null)
                _placingItem.DestroyGO();

            _buildItemData = buildItemData;
            _placingItem = Object.Instantiate(_buildItemData.PlacingItem, _placingItemParent);
            _placingItem.Init(_inputSystem, _cameraSystem);
            PlacingStarted?.Invoke();
        }

        public void AcceptPlacing()
        {
            if (_placingItem == null)
                return;

            var build = Object.Instantiate(_buildItemData.BuildPrefab, _buildsParent);
            build.transform.position = _placingItem.transform.position;

            _builds.Add(build);
            build.Init(_gameContext);

            _placingItem.DestroyGO();
            PlacingCanceled?.Invoke();
            PlacingCompleted?.Invoke();
        }

        public void CancelPlacing()
        {
            if (_placingItem == null)
                return;

            _placingItem.DestroyGO();
            PlacingCanceled?.Invoke();
        }

        public void RotatePlacingItem()
        {
            if (_placingItem == null)
                return;

            _placingItem.Rotate();
        }

        public void DestroyAllBuilds()
        {
            _builds.ForEach(build => build.DestroyGO());
            _builds.Clear();
        }
    }
}