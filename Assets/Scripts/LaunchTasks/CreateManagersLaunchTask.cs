using Core.GameContext;
using Core.GameLaunch;
using Core.Systems.UI;
using Game.Towers;
using UnityEngine;

namespace LaunchTasks
{
    [CreateAssetMenu(menuName = "Data/Launch Tasks/Create Managers", fileName = "Create Managers Launch Task")]
    public class CreateManagersLaunchTask : LaunchTask
    {
        public override void Execute(IGameContext gameContext)
        {
            CreateCoreManagers(gameContext);
            CreateGameManager(gameContext);
        }

        private void CreateCoreManagers(IGameContext gameContext)
        {
            var uiManager = new UISystemManager();

            gameContext.AddSystem<IUISystemManager>(uiManager);
        }

        private void CreateGameManager(IGameContext gameContext)
        {
            var towerManager = new TowerManager();

            gameContext.AddManager<ITowerManager>(towerManager);
        }
    }
}