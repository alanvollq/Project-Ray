using Core.GameContext;
using UnityEngine;

namespace Game.Systems.BuildSystem
{
    public abstract class Build : MonoBehaviour
    {
        protected IGameContext GameContext { get; private set; }


        public void Init(IGameContext gameContext)
        {
            GameContext = gameContext;
            OnInit();
        }

        protected virtual void OnInit(){}
    }
}